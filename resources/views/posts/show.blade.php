@extends('layouts.app')

@section('content')

    <a href="/posts/" class="btn btn-light"><i class="fas fa-arrow-left"></i> Go back</a>
    <hr>
    <div class="card mb-3">
        @if ($post->cover_image === 'noimage.jpg')
            <img class="card-img-top" src="http://via.placeholder.com/700x300" alt="Post main image">
        @else
            <img class="card-img-top" src="/storage/cover_images/{{ $post->cover_image }}" alt="Post main image">
        @endif
    </div>
    
    <h1>{{ $post->title }}</h1>
    <p class="text-muted">Written on {{ $post->created_at }} by {{ $post->user->name }} </p>
    <hr>

    <div>
        {!! $post->body !!}
    </div>

    @guest

    @else
        @if ($post->user_id === auth()->user()->id)

        <hr>
            
            <a href="/posts/{{$post->id}}/edit" class="btn btn-info">Edit post</a>

            {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right']) !!}

            {!! Form::hidden('_method', 'DELETE') !!}
            {!! Form::submit('Delete Post', ['class' => 'btn btn-danger']) !!}

            {!! Form::close() !!}

        @endif
    @endguest

@endsection