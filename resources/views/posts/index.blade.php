@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    <hr>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card mb-3 border-dark">
                @if ($post->cover_image === 'noimage.jpg')
                    <img class="card-img-top" src="http://via.placeholder.com/700x200" style="object-fit: cover; object-position: 50% 50%;" height="200"  alt="Post main image">
                @else
                    <img class="card-img-top" src="/storage/cover_images/{{ $post->cover_image }}" style="object-fit: cover; object-position: 50% 50%;" height="200" alt="Post main image">
                @endif

                <div class="card-body">
                    <h3 class="card-title">{{ $post->title }}</h3>
                    <p class="text-muted">Written on {{ $post->created_at }} by {{ $post->user->name }} </p>                    
                    <a href="/posts/{{ $post->id }}" class="btn btn-primary float-right">Read Post</a>
                </div>
            </div>
        @endforeach

        <div class="pagination justify-content-center">
            {{ $posts->links() }}
        </div>

    @else
        <div class="alert alert-danger">
            No posts found
        </div>
    @endif
@endsection