@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <h2>Welcome back, {{ auth()->user()->name }} </h2>
                    <hr>
                    <a href="/posts/create" class="btn btn-primary btn-large">Create Post</a>
                    <hr>

                    @if(count($posts) > 0)

                    <h3>Your posts</h3>

                    <ul class="list-group">
                        @foreach($posts as $post)

                        <a href="/posts/{{ $post->id }}" class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $post->title }} ( {{ $post->created_at }} )
                        </a>


                        @endforeach
                    </ul>

                    @else 
                        <div class="alert alert-danger">
                            You don't have any posts!
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
