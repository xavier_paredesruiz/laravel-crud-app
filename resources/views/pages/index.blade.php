@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center">
        <h1> {{ $title }} </h1>
        <p>This is the Laravel application from the "Laravel from Scratch" YouTube Series</p>
        <p><a class="btn btn-primary btn-large" role="button" href="/login">Login</a> <a href="/register" class="btn btn-success btn-large">Register</a> </p>        
    </div>
@endsection